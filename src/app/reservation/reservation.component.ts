import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { ReservationQuery } from "../models/reservation-query.model";
import { ReservationService } from "../services/reservation.service";
import { isEmpty } from "lodash";
import { Reservation } from "../models/reservation.model";

@Component({
  selector: "app-reservation",
  templateUrl: "./reservation.component.html",
  styleUrls: ["./reservation.component.css"]
})
export class ReservationComponent implements OnInit {
  createForm: FormGroup;
  showNotFound: boolean = false;
  reservation: Reservation;
  reservationQuery: ReservationQuery = new ReservationQuery();
  reservationResponse: [Reservation];

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private reservationService: ReservationService
  ) {
    this.createForm = this.fb.group({
      identificationCard: ["", Validators.required]
    });
  }

  getReservation() {
    this.reservationService
      .getReservationByIdentificationCard(
        this.reservationQuery.identificationCard
      )
      .subscribe(data => {
        if (isEmpty(data)) {
          this.showNotFound = true;
        } else {
          this.showNotFound = false;
          this.reservationResponse = <[Reservation]>data;
        }
      });
  }

  ngOnInit() {}
}
