import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: "getService" })
export class GetServicePipe implements PipeTransform {
  transform(value: string, exponent: string): string {
    switch (value) {
      case "ECONOMIC": {
        return "Economico";
      }
      case "EXECUTIVE": {
        return "Ejecutivo";
      }
      default: {
        return value;
      }
    }
  }
}
