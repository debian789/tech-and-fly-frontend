import { Component, OnInit } from "@angular/core";
import { FlightService } from "../services/flight.service";
import { Flight } from "../models/flight.models";
import { isEmpty } from "lodash";

@Component({
  selector: "app-flights",
  templateUrl: "./flights.component.html",
  styleUrls: ["./flights.component.css"]
})
export class FlightsComponent implements OnInit {
  flights: Flight[];
  showNotFound: boolean = true;

  constructor(private flightServices: FlightService) {}

  ngOnInit() {
    this.flightServices.getFlight().subscribe(data => {
      if (!isEmpty(data)) {
        this.showNotFound = false;
        this.flights = <Flight[]>data["rows"];
      } else {
        this.showNotFound = true;
      }
    });
  }

  reservation(id) {}
}
