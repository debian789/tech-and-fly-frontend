import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReservationComponent } from './reservation/reservation.component';
import { RegistrationFormComponent } from './registration-form/registration-form.component'
import { MatDialogModule, MatNativeDateModule, MatSnackBarModule, MatDividerModule, MatTableModule, MatSelectModule, MatOptionModule, MatFormFieldModule,MatToolbarModule,MatInputModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatCardModule } from '@angular/material'
import {ReservationService} from './services/reservation.service'
import {GetServicePipe} from './pipes/service.pipes'
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'; 
import {FlexLayoutModule} from '@angular/flex-layout';
import { FlightsComponent } from './flights/flights.component';
import { FlightService } from './services/flight.service';
import { DialogComponent } from './dialog/dialog.component';
import { MatDatepickerModule, MatMomentDateModule } from '@coachcare/datepicker';

@NgModule({
  declarations: [
    AppComponent,
    ReservationComponent,
    GetServicePipe,
    RegistrationFormComponent,
    FlightsComponent,
    DialogComponent
  ],
  imports: [
    FlexLayoutModule,
    HttpClientModule,
    BrowserModule,
    MatDialogModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatSidenavModule,
    MatFormFieldModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    MatIconModule,
    MatDatepickerModule,
    FormsModule,    
    MatNativeDateModule,
    MatListModule,
    MatButtonModule,
    MatCardModule,
    MatTableModule,
    MatDividerModule,
    MatSnackBarModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatMomentDateModule
  ],
  providers: [ReservationService,FlightService],
  bootstrap: [AppComponent],
  entryComponents: [DialogComponent]
})
export class AppModule { }
