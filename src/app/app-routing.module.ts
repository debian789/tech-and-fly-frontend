import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ReservationComponent} from './reservation/reservation.component'
import {RegistrationFormComponent} from './registration-form/registration-form.component'
import { FlightsComponent } from './flights/flights.component';

const routes: Routes = [
  {
    path: '',
    component: FlightsComponent
  },
  {
    path: 'create-reservation',
    component: RegistrationFormComponent
  },
  {
    path: 'reservations',
    component: ReservationComponent
  }  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
