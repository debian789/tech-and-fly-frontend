import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { ReservationQuery } from "../models/reservation-query.model";
import { ReservationResponse } from "../models/reservation-response.model";
import { ReservationService } from "../services/reservation.service";
import { isEmpty } from "lodash";
import { Reservation } from "../models/reservation.model";
import { MatDialog, MatDialogConfig } from "@angular/material";
import { DialogComponent } from "../dialog/dialog.component";
import { iconsList } from "../commons/contants";

@Component({
  selector: "app-create",
  templateUrl: "./registration-form.component.html",
  styleUrls: ["./registration-form.component.css"]
})
export class RegistrationFormComponent implements OnInit {
  createForm: FormGroup;
  showNotFound: boolean = false;
  reservationSave: Reservation;
  reservationQuery: ReservationQuery = new ReservationQuery();
  reservationResponse: [ReservationResponse];

  constructor(
    private dialog: MatDialog,
    private fb: FormBuilder,
    private router: Router,
    private reservationService: ReservationService
  ) {
    this.createForm = this.fb.group({
      originCity: ["", Validators.required],
      destinationCity: ["", Validators.required],
      servicesType: ["", Validators.required],
      adults: ["", Validators.required],
      children: ["", Validators.required],
      babies: ["", Validators.required],
      identificationCard: ["", Validators.required],
      age: ["", Validators.required]
    });
  }

  sarchFlight() {
    const reservationData = this.reservationBuild(this.reservationQuery);

    this.reservationService
      .getAvalibleFlight(reservationData)
      .subscribe(data => {
        if (isEmpty(data)) {
          this.showNotFound = true;
        } else {
          this.showNotFound = false;
          this.reservationResponse = <[ReservationResponse]>data;
        }
      });
  }

  reservationBuild(reservationBuild: ReservationQuery): ReservationQuery {
    const result = {};
    const numberPerson: number =
      Number(reservationBuild.adults) +
      Number(reservationBuild.children) +
      Number(reservationBuild.babies);

    if (reservationBuild.servicesType === "ECONOMIC") {
      reservationBuild.serviceEconomic = numberPerson;
      reservationBuild.serviceEjecutive = 0;
    } else {
      reservationBuild.serviceEconomic = 0;
      reservationBuild.serviceEjecutive = numberPerson;
    }

    return reservationBuild;
  }

  ngOnInit() {}

  openDialog(icon, menssage) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = { icon, menssage };
    const dialogRef = this.dialog.open(DialogComponent, dialogConfig);
    dialogRef
      .afterClosed()
      .subscribe(val => console.log("Dialog output:", val));
  }

  activeTemp() {
    this.openDialog(iconsList.checkCircle, "Reservación realizada");
  }

  reservation(id, cost) {
    this.reservationSave = new Reservation();
    this.reservationSave.adults = this.reservationQuery.adults;
    this.reservationSave.age = this.reservationQuery.age;
    this.reservationSave.babies = this.reservationQuery.babies;
    this.reservationSave.cost = cost;
    this.reservationSave.departureDate = this.reservationQuery.checkOutTime;
    this.reservationSave.children = this.reservationQuery.children;
    this.reservationSave.destinationCity = this.reservationQuery.destinationCity;
    this.reservationSave.identificationCard = this.reservationQuery.identificationCard;
    this.reservationSave.originCity = this.reservationQuery.originCity;
    this.reservationSave.service = this.reservationQuery.servicesType;
    this.reservationSave.returnDate = this.reservationQuery.timeOfArrival;
    this.reservationSave.flight = id;

    this.reservationService
      .createReservation(this.reservationSave)
      .subscribe(res => {
        if (isEmpty(res)) {
          this.openDialog(
            iconsList.warning,
            "Inconsistencias al guardar la información"
          );
        } else {
          if (res['code'] === 1) {
            this.openDialog(iconsList.warning, "Solo se permite una reserva por dia");
          } else {
            this.openDialog(iconsList.checkCircle, "Reservación realizada");
          }
        }
      });
  }
}
