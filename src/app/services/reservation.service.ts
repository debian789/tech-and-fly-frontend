import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
// import {Observable} from "rxjs/Observable";
import { map } from "rxjs/operators";
import { Reservation } from "../models/reservation.model";
import { Observable } from "rxjs";
import { ReservationQuery } from "../models/reservation-query.model";
import { environment } from "../../environments/environment";

@Injectable()
export class ReservationService {
  constructor(private http: HttpClient) {}
  url = environment.urlServices;
  getReservationByIdentificationCard(id) {
    return this.http.get(`${this.url}/reservations/${id}`);
  }

  getAvalibleFlight(data: ReservationQuery) {
    return this.http.post(`${this.url}/flights/search`, data);
  }

  createReservation(data: Reservation) {
    return this.http.post(`${this.url}/reservations`, data);
  }
}
