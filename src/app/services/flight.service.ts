import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Observable } from "rxjs";
import { environment } from "../../environments/environment";

@Injectable()
export class FlightService {
  constructor(private http: HttpClient) {}
  url = environment.urlServices;

  getFlight() {
    return this.http.get(`${this.url}/flights`);
  }
}
