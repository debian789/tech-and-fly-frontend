export class Reservation {
  departureDate: Date;
  returnDate: Date;
  originCity: String;
  destinationCity: String;
  service: String;
  adults: Number;
  children: Number;
  babies: Number;
  identificationCard: Number;
  age: Number;
  flight: String;
  cost: number;
}
