export class ReservationResponse {
  originCity: String;
  destinationCity: String;
  checkOutTime: Date;
  timeOfArrival: Date;
  status: String;
  serviceEconomic: number;
  serviceEjecutive: number;
  adults: number;
  children: number;
  babies: number;
  cost: String;
  code: String;
}
