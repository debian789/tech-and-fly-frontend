export class ReservationQuery {
  identificationCard: number;
  originCity: String;
  destinationCity: String;
  checkOutTime: Date;
  timeOfArrival: Date;
  servicesType: String;
  serviceEconomic: number;
  serviceEjecutive: number;
  adults: number;
  children: number;
  babies: number;
  age: number;
  cost: number;
}
