import {Component, Inject, OnInit, ViewEncapsulation} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit {
    icon:string;
    menssage: string;

    constructor(
        private dialogRef: MatDialogRef<DialogComponent>,
        @Inject(MAT_DIALOG_DATA) {icon, menssage}:any ) {
        this.icon = icon;
        this.menssage = menssage;      
    }

    ngOnInit() {

    }

    save() {
        this.dialogRef.close();
    }

    close() {
        this.dialogRef.close();
    }
}