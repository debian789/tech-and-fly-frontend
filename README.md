# TechAndFly

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

## Desplegar servidor

Correr el comando `ng serve` para desplegar el server en modo desarrollo. 

Ingresando a la url  `http://localhost:4200/`. se puede acceder al proyecto.


## Constuir

Correr el comando  `ng build` construye el proyecto generando en la carpeta  `dist/` con los archivos para el despliegue en el servidor, al implementar `--prod`  construye con archivos minificados.

## Actualizar url de despliegue 

En el archivo environments.ts y environments.prod.ts se puede modificar el valor **urlServices** para actualizar la referencia del servidor api que se va a conectar el aplicativo web   